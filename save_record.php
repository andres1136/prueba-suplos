<?php
    $user = 'root'; //Cambiar Usuario si es necesario
    $pass = ''; //Digitar contraseña si es necesario
    // $pass = 'pass123';

    try {
        $conn = new PDO('mysql:host=localhost;dbname=Intelcost_bienes;charset=utf8', $user, $pass);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
        if(isset($_POST['data'])) {
            $data = $_POST['data'];
            $res = json_decode($data, true);
            $sql =  'INSERT INTO bienes(id, direccion, ciudad, telefono, codigo_postal, tipo, precio)';
            $sql .= ' VALUES (NULL, :direccion, :ciudad, :telefono, :codigo_postal, :tipo, :precio)';
            $stmt = $conn->prepare($sql);
            $stmt->bindValue(':direccion', $res['Direccion'], PDO::PARAM_STR);
            $stmt->bindValue(':ciudad', $res['Ciudad'], PDO::PARAM_STR);
            $stmt->bindValue(':telefono', $res['Telefono'], PDO::PARAM_STR);
            $stmt->bindValue(':codigo_postal', $res['Codigo_Postal'], PDO::PARAM_STR);
            $stmt->bindValue(':tipo', $res['Tipo'], PDO::PARAM_STR);
            $stmt->bindValue(':precio', $res['Precio'], PDO::PARAM_STR);

            if($stmt->execute()){
                echo json_encode("Datos insertados con exito");
            }else{
                echo json_encode("Error");
            }
        } else {
            echo json_encode("Error");
        }
    } catch(PDOException $e) {
        echo 'ERROR: ' . $e->getMessage();
    }
?>