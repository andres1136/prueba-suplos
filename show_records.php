<?php
    $user = 'root'; //Cambiar Usuario si es necesario
    $pass = ''; //Digitar contraseña si es necesario
    // $pass = 'pass123';

    try {
        $conn = new PDO('mysql:host=localhost;dbname=Intelcost_bienes;charset=utf8', $user, $pass);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);   
        $stmt = $conn->prepare('SELECT * FROM bienes');
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($result);
    } catch(PDOException $e) {
        echo 'ERROR: ' . $e->getMessage();
    }
?>