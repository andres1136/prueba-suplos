$(document).ready(function() { 
    $('#bienes').click(function() {
        $.ajax({
            url: 'show_records.php',
            type: "GET",
            timeout: 6000,// add if using post
            dataType : 'json', //text
            crossDomain: false,
            cache: false,
            async: true,
            success: function(data) {
                if(data.length > 0) {
                    $('#listaBienes').html("");
                    $.each(data, function(index, element) {
                        var Items = $('<div class="col s12 m7 cardHouse">'+
                        '<div class="card horizontal">'+
                            '<div class="card-image">'+
                                    '<img src="img/home.jpg" />'+
                                '</div>'+
                                '<div class="card-stacked items">'+
                                    '<div class="card-content">'+
                                        '<ul>'+
                                            '<li><b>Dirección: </b>'+element.direccion+'</li>'+
                                            '<li><b>Ciudad: </b>'+element.ciudad+'</li>'+
                                            '<li><b>Telefóno: </b>'+element.telefono+'</li>'+
                                            '<li><b>Codigo postal: </b>'+element.codigo_postal+'</li>'+
                                            '<li><b>Tipo: </b>'+element.tipo+'</li>'+
                                            '<li><b>Precio: </b>'+element.precio+'</li>'+
                                        '</ul>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="divider"></div>');
                        $('#listaBienes').append(Items);
                    });
                } else {
                    $('#listaBienes').html("");
                    var message = $('<h4>No hay elementos para mostrar</h4>');
                    $('#listaBienes').append(message);
                }
            }                      
        });
    }); 
});