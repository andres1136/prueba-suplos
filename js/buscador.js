$(document).ready(function() {
    $('#submitButton').click(function(event) {
        event.preventDefault();
        var ciudad = $('#selectCiudad').val();
        var tipo =  $('#selectTipo').val();
        if (ciudad !== '' && tipo !== 0) {
            $.getJSON('./data-1.json', function(data) {
                var result = data.filter(function(el) {
                    return el.Ciudad === ciudad && el.Tipo === tipo;
                });
                if (result.length > 0) {
                    console.log('Entro aca');
                    $("#listaBusqueda").html("");
                    $.each(result, function(index, element) {
                        var Items = $('<div class="col s12 m7 cardHouse">'+
                            '<div class="card horizontal">'+
                                '<div class="card-image">'+
                                    '<img src="img/home.jpg" />'+
                                '</div>'+
                                '<div class="card-stacked items">'+
                                    '<div class="card-content">'+
                                        '<ul>'+
                                            '<li><b>Dirección: </b>'+element.Direccion+'</li>'+
                                            '<li><b>Ciudad: </b>'+element.Ciudad+'</li>'+
                                            '<li><b>Telefóno: </b>'+element.Telefono+'</li>'+
                                            '<li><b>Codigo postal: </b>'+element.Codigo_Postal+'</li>'+
                                            '<li><b>Tipo: </b>'+element.Tipo+'</li>'+
                                            '<li><b>Precio: </b>'+element.Precio+'</li>'+
                                        '</ul>'+
                                    '</div>'+
                                    '<div class="card-action">'+
                                        '<button data-id="'+element.Id+'" class="waves-effect waves-light btn saveItem">Guardar</button>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="divider"></div>');
                        $('#listaBusqueda').append(Items);
                    });
                    $('#totalRecords').text(function() { 
                        return 'Resultados de la búsqueda: ' + result.length;
                    });
                }   
            });
        } else { 
            alert('Llena los campos');
            $.getJSON('./data-1.json', function(data) {
                $.each(data, function(index, element) {
                    var Items = $('<div class="col s12 m7 cardHouse">'+
                                        '<div class="card horizontal">'+
                                            '<div class="card-image">'+
                                                '<img src="img/home.jpg" />'+
                                            '</div>'+
                                            '<div class="card-stacked items">'+
                                                '<div class="card-content">'+
                                                    '<ul>'+
                                                        '<li><b>Dirección: </b>'+element.Direccion+'</li>'+
                                                        '<li><b>Ciudad: </b>'+element.Ciudad+'</li>'+
                                                        '<li><b>Telefóno: </b>'+element.Telefono+'</li>'+
                                                        '<li><b>Codigo postal: </b>'+element.Codigo_Postal+'</li>'+
                                                        '<li><b>Tipo: </b>'+element.Tipo+'</li>'+
                                                        '<li><b>Precio: </b>'+element.Precio+'</li>'+
                                                    '</ul>'+
                                                '</div>'+
                                                '<div class="card-action">'+
                                                    '<button data-id="'+element.Id+'" class="waves-effect waves-light btn saveItem">Guardar</button>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="divider"></div>');
                                    $('#listaBusqueda').append(Items);
        
                });
                $('#totalRecords').text(function() { 
                    return 'Resultados de la búsqueda: ' + data.length;
                });
            });
        }
    });
});