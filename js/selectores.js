$(document).ready(function() {
    $.getJSON('./data-1.json', function(data) {
        var placeholder = $('<option value="" selected>Elige un tipo</option>');
        $('#selectTipo').append(placeholder);
        $.each(data, function(index, element) {
            var city = $('<option value="'+element.Ciudad+'">'+element.Ciudad+'</option>')
            var type = $('<option value="'+element.Tipo+'">'+element.Tipo+'</option>')
            $('#selectCiudad').append(city);
            $('#selectTipo').append(type);
        });
        var found = [];
        $("#selectCiudad option").each(function() {
          if($.inArray(this.value, found) != -1) $(this).remove();
          found.push(this.value);
        });
        $("#selectTipo option").each(function() {
            if($.inArray(this.value, found) != -1) $(this).remove();
            found.push(this.value);
        });
    });
});


